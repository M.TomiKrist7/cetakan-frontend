import { Component } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ApiServiceService } from 'src/service/api-service.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  hide=true;
  loginForm!: FormGroup;
  checkPassword=false;




  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private http:HttpClient,
    private apiService:ApiServiceService,
  ){}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: [null, [Validators.required]],
      password: [null, [Validators.required]]
    })
  }

  get password(): any {return this.loginForm.get('password')}

  onLogin(data:any){

    if(this.loginForm.valid){
      this.apiService.postDataApi('users/login',this.loginForm.value).subscribe(
        (res)=>{
          if(res.success === false){
              // console.log('Password Salah');
              this.checkPassword = true;
              this.password.reset()
              console.log(this.checkPassword)
          }else{
            // console.log(res)
            sessionStorage.setItem('auth',res.token)
            this.router.navigate(['/beranda'])
          //console.log(res.data.token)
          }
        }
      )
    }else{
      this.validateFrom(this.loginForm)
    }
  }


  private validateFrom(fromGroup:FormGroup){
    Object.keys(fromGroup.controls).forEach(field=>{
      const control = fromGroup.get(field);

      if(control instanceof FormControl){
        control.markAsDirty({onlySelf:true})
      } else if (control instanceof FormGroup) {
        this.validateFrom(control);
      }
    })
  }

}
