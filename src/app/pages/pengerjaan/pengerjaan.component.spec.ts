import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PengerjaanComponent } from './pengerjaan.component';

describe('PengerjaanComponent', () => {
  let component: PengerjaanComponent;
  let fixture: ComponentFixture<PengerjaanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PengerjaanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PengerjaanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
