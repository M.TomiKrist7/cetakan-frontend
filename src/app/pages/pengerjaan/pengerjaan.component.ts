import { Component, OnInit, AfterViewInit, ViewChild, OnDestroy } from '@angular/core';
import {LiveAnnouncer} from '@angular/cdk/a11y';
import { Subscription } from 'rxjs';
import { ApiServiceService } from 'src/service/api-service.service';
import {MatSort, Sort, MatSortModule} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { cibTheMovieDatabase } from '@coreui/icons';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { HttpErrorResponse } from '@angular/common/http';

export interface dataPermintaan {
  Produk:string;
  Deskripsi:string;
  ReceivedDate:Date;
  MenuName:string;
  Customer:string;
  User:string;
  PaperType:string;
  DeadLine:Date;
  DataBase:string;
}



@Component({
  selector: 'app-pengerjaan',
  templateUrl: './pengerjaan.component.html',
  styleUrls: ['./pengerjaan.component.css']
})
export class PengerjaanComponent implements OnInit {


  dataRequest:dataPermintaan[]=[]
  displayedColumns: string[] =[
    'Produk',
    'Deskripsi',
    'ReceivedDate',
    'MenuName',
    'Customer',
    'User',
    'PaperType',
    'DeadLine',
    'DataBase'];

  dataSource = new MatTableDataSource<dataPermintaan>(this.dataRequest);

  constructor(private ApiService:ApiServiceService,private _liveAnnouncer: LiveAnnouncer){}

  @ViewChild(MatSort,{static:true}) sort!: MatSort;

  // ngAfterViewInit() {
  //   this.dataRequest.sort = this.sort;
  // }

  ngOnInit():void{
        this.ApiService.getDataApi('permintaan/').subscribe(
          (resp) =>{
            // console.log(resp.data);
            this.dataRequest = resp;
            console.log(this.dataRequest)
          },
          (err)=>{

          }

        )



  }

  // getDataRequest(){
  //   this.ApiService.getDataApi('permintaan/').subscribe(res=>{
  //     this.dataRequest=new MatTableDataSource(res);
  //     console.log(this.dataRequest);
  //   })
  // }

  clickedRows = new Set<dataPermintaan>();
  onSearch(){

  }

  announceSortChange(sortState: Sort) {
    // This example uses English messages. If your application supports
    // multiple language, you would internationalize these strings.
    // Furthermore, you can customize the message to add additional
    // details about the values being sorted.
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }
}

