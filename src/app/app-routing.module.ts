import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';
import { LoginComponent } from './pages/login/login.component';
import { ShellComponent } from './shared/shell/shell.component';
import { BerandaComponent } from './pages/beranda/beranda.component';
import { JadwalComponent } from './pages/jadwal/jadwal.component';
import { PengerjaanComponent } from './pages/pengerjaan/pengerjaan.component';

const routes: Routes = [
  {
    path:'',
    component:LoginComponent,

  },
  {
    path:'',
    component:ShellComponent,
    canActivate:[AuthGuard],
    children:[
        {
          path:'beranda',
          canActivate:[AuthGuard],
          component:BerandaComponent,
        },
        {
          path:'jadwal',
          canActivate:[AuthGuard],
          component:JadwalComponent,
        },
        {
          path:'pengerjaan',
          canActivate:[AuthGuard],
          component:PengerjaanComponent,
        },
    ]
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
