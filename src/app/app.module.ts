import { NgModule,LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormGroup, FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import { HttpClientModule } from '@angular/common/http';
import { IconModule, IconSetService } from '@coreui/icons-angular';
import { RouterModule } from '@angular/router';
import { SidebarModule } from '@syncfusion/ej2-angular-navigations';
import { NgApexchartsModule } from 'ng-apexcharts';
import { CalendarModule, DateAdapter, MOMENT } from 'angular-calendar';
import {MatTableModule,MatTableDataSource} from '@angular/material/table';
import {MatSort, Sort} from '@angular/material/sort';
import { DataTablesModule } from "angular-datatables";


import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { registerLocaleData } from '@angular/common';
import localeIt from '@angular/common/locales/it';
registerLocaleData(localeIt);
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import localeId from '@angular/common/locales/id';
registerLocaleData(localeId, 'id');

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { BerandaComponent } from './pages/beranda/beranda.component';
import { ShellComponent } from './shared/shell/shell.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { JadwalComponent } from './pages/jadwal/jadwal.component';
import { PengerjaanComponent } from './pages/pengerjaan/pengerjaan.component';
import { DayMaterialComponent } from './material/day-material/day-material.component';
import { MonthHeaderComponent } from './modules/month/month-header/month-header/month-header.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BerandaComponent,
    ShellComponent,
    SidebarComponent,
    JadwalComponent,
    PengerjaanComponent,
    MonthHeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatIconModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    HttpClientModule,
    IconModule,
    SidebarModule,
    RouterModule.forRoot([],{initialNavigation:'enabledNonBlocking'}),
    NgApexchartsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    MatProgressSpinnerModule,
    FormsModule,
    NgbModalModule,
    MatTableModule,
    DataTablesModule,










  ],
  providers: [IconSetService,MonthHeaderComponent,{ provide: LOCALE_ID, useValue: "id-ID" },MatTableModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
