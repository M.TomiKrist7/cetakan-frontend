import { TestBed } from '@angular/core/testing';

import { CalendarUtilsProviderService } from './calendar-utils.provider.service';

describe('CalendarUtilsProviderService', () => {
  let service: CalendarUtilsProviderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalendarUtilsProviderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
