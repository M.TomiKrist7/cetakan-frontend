import { NgModule, Injectable } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  GetMonthViewArgs,
  MonthView,
  GetWeekViewHeaderArgs,
  WeekDay,
  GetWeekViewArgs,
  WeekView,
  getMonthView,
  getWeekViewHeader,
  getWeekView,
} from 'calendar-utils';
import {DateAdapter} from 'angular-calendar'

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})

@Injectable()
export class CalendarUtilsProviderModule {
  constructor(protected dateAdapter: DateAdapter) {}

  getMonthView(args: GetMonthViewArgs): MonthView {
    return getMonthView(this.dateAdapter, args);
  }

  getWeekViewHeader(args: GetWeekViewHeaderArgs): WeekDay[] {
    return getWeekViewHeader(this.dateAdapter, args);
  }

  getWeekView(args: GetWeekViewArgs): WeekView {
    return getWeekView(this.dateAdapter, args);
  }
 }
