import {
  Component,
  Input,
  TemplateRef,
  EventEmitter,
  Output,
} from '@angular/core';
import { WeekDay } from 'calendar-utils';
import { trackByWeekDayHeaderDate } from 'src/app/utils'

@Component({
  selector: 'app-month-header',
  templateUrl: './month-header.component.html',
  styleUrls: ['./month-header.component.css']
})
export class MonthHeaderComponent {
  @Input() days!: WeekDay[];

  @Input() locale!: string;

  @Input() customTemplate!: TemplateRef<any>;

  @Output() columnHeaderClicked = new EventEmitter<{
    isoDayNumber: number;
    sourceEvent: MouseEvent;
  }>();

  trackByWeekDayHeaderDate = trackByWeekDayHeaderDate;

}
