import { Injectable } from '@angular/core';
import { HttpClient,HttpParams, HttpErrorResponse, } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  APP_URL = environment.APP_URL;
  token = sessionStorage.getItem('auth');
  headers = new HttpHeaders()
            .set('Content-Type', 'application/json; charset=utf-8')
            .set('Accept', 'application/json')
            .set('Authorization', `Bearer ${this.token}`);
  isLoading = new BehaviorSubject<boolean>(false);

  constructor( private httpClient: HttpClient,
    private router: Router) { }

    static isLoggedin():boolean {
      return!!sessionStorage.getItem('auth');
    }


    getDataApi(param: string, isParam?: boolean): Observable<any> {
      return this.httpClient
        .get<any>(this.APP_URL + param, {
          headers: this.headers,
          responseType: 'json'
        })
        .pipe(catchError(this.errorGetHandler.bind(this)));
    }

    postDataApi(param: string, body: any, isParam?: boolean): Observable<any> {
      console.log(param)
      return this.httpClient
        .post<any>(this.APP_URL + param, body, {
          headers: this.headers
        })
        .pipe(catchError(this.errorHandler));
    }


    errorHandler(error: HttpErrorResponse) {
      return throwError(error.message || 'Data Not Found');
    }

    errorGetHandler(error: HttpErrorResponse) {
      this.router.navigate(['/not-found']);
      return throwError(error.message || 'Data Not Found');
    }


}

