/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,ts}",],
  theme: {
    extend: {
      backgroundColor:{
        'magen':'#ef476f',
        'kuning':'#ffd166',
        'hijau':'#06d6a0',
        'biru':'#118ab2',
        'gelap':'#073b4c'
      },
      colors:{
        'magen':'#ef476f',
        'kuning':'#ffd166',
        'hijau':'#06d6a0',
        'biru':'#118ab2',
        'gelap':'#073b4c'
      },
      borderColor:{
        'magen':'#ef476f',
        'kuning':'#ffd166',
        'hijau':'#06d6a0',
        'biru':'#118ab2',
        'gelap':'#073b4c'
      }
    },
  },
  plugins: [],
}
